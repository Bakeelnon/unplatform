﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UN_PlayerController : MonoBehaviour
{

    new Rigidbody2D rigidbody2D;
    GameObject spawner;
    public Animator animator;
    public GameObject blood;
    public GameObject Panel;

    public float DieRate = 100f;
    private float DieNext = 0f;
    private float maxSpeed = 22;
    private float movespeed = 3000f;
    private float Acceleration = 600;
    private float jumpSpeed = 23;
    private float JumpDuration = 180;

    private bool EnableDoubleJmp = true;
    private bool wallHitDoubleJumpOverride = true;
    private bool facingRight;
    private bool StartTime = false;
    private bool isMenuToogled = false;
    public bool Died = false;
    

    //Checks
    bool canDoubleJump = true;

    float jmpDuration;

    bool jumpKeyDown = false;
    bool canVariableJump = false;



    void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        spawner = GameObject.Find("Spawner");
        facingRight = true;
    }


    void FixedUpdate()
    {
        bool leftWallHit = isOnWallLeft();
        bool rightWallHit = isOnWallRigth();
        bool onGround = isOnGround();
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxisRaw("Horizontal");

        if (vertical > 0.1f)
        {
            animator.SetBool("IsJumping", true);
        }
        else
        {
            animator.SetBool("IsJumping", false);
        }

        if (!onGround)
        {
            animator.SetBool("IsFalling", true);
        }
        else
        {
            animator.SetBool("IsFalling", false);
        }

        if (rightWallHit & horizontal > 0)
        {
            animator.SetBool("IsJumping", false);
            animator.SetBool("IsWallTouched", true);
        }
        else if (leftWallHit & horizontal < 0)
        {
            animator.SetBool("IsJumping", false);
            animator.SetBool("IsWallTouched", true);
        }
        else
        {
            animator.SetBool("IsWallTouched", false);
        }


        //Debug.Log(Died);
    }

    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");

        //Flip player sprite if it's neseccery
        Flip(horizontal);

        //update animator controller variable
        animator.SetFloat("Speed", Mathf.Abs(horizontal));

        if (Input.GetKeyUp(KeyCode.A) && isOnGround())
            rigidbody2D.Sleep();

        if (Input.GetKeyUp(KeyCode.D) && isOnGround())
            rigidbody2D.Sleep();

        if (Input.GetKey(KeyCode.A))
            Move(-1);

        if (Input.GetKey(KeyCode.D))
            Move(1);

        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (!isMenuToogled)
            {
                Panel.SetActive(true);
                isMenuToogled = true;
            }
            else
            {
                Panel.SetActive(false);
                isMenuToogled = false;
            }
        }


        bool onTheGround = isOnGround();

        float vertical = Input.GetAxisRaw("Vertical");

        if (onTheGround)
        {
            canDoubleJump = true;
            animator.SetBool("IsJumping", false);
        }

        if(vertical > 0.1f)
        {
            if (!jumpKeyDown) //1st frame
            {
                jumpKeyDown = true;

                if(onTheGround || (canDoubleJump && EnableDoubleJmp) || wallHitDoubleJumpOverride)
                {
                    bool wallHit = false;
                    int wallHitDirection = 0;

                    bool leftWallHit = isOnWallLeft();
                    bool rightWallHit = isOnWallRigth();

                    
                    if(horizontal != 0)
                    {
                        if (leftWallHit)
                        {
                            wallHit = true;
                            wallHitDirection = 1;
                        }
                        else if (rightWallHit)
                        {
                            wallHit = true;
                            wallHitDirection = -1;
                        }
                    }

                    if (!wallHit)
                    {
                        if (onTheGround || (canDoubleJump && EnableDoubleJmp))
                        {
                            rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, this.jumpSpeed);

                            JumpDuration = 0.0f;
                            canVariableJump = true;
                        }
                    }
                    else
                    {
                        rigidbody2D.velocity = new Vector2(this.jumpSpeed * wallHitDirection, 35f);
                        jmpDuration = 0.0f;
                        canVariableJump = true;
                    }

                    if(!onTheGround && !wallHit)
                    {
                        canDoubleJump = false;
                    }
                }
            }//2nd frame
            else if (canVariableJump)
            {
                jmpDuration += Time.deltaTime;
                if(jmpDuration < this.JumpDuration / 1000)
                {
                    rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, this.jumpSpeed);
                }
            }
        }
        else
        {
            jumpKeyDown = false;
            canVariableJump = false;
        }
        
    }
    

    private bool isOnGround()
    {
        float lengthToSearch = 0.1f;
        float colliderTheshold = 0.001f;

        Vector2 linestart = new Vector2(this.transform.position.x, this.transform.position.y - GetComponent<Renderer>().bounds.extents.y - colliderTheshold);

        Vector2 vectorToSearch = new Vector2(this.transform.position.x, linestart.y - lengthToSearch);

        RaycastHit2D hit = Physics2D.Linecast(linestart, vectorToSearch);

        return hit;
    }

    private bool isOnWallLeft()
    {
        bool retVal = false;
        float lenthToSearch = 0.1f;
        float colliderThreshold = 0.01f;

        Vector2 linestart = new Vector2(this.transform.position.x - GetComponent<Renderer>().bounds.extents.x - colliderThreshold, this.transform.position.y);

        Vector2 vectorToSearch = new Vector2(linestart.x - lenthToSearch, this.transform.position.y);

        RaycastHit2D hitLeft = Physics2D.Linecast(linestart, vectorToSearch);

        retVal = hitLeft;

        if (retVal)
        {
            if (hitLeft.collider.GetComponent<NoSlideJump>())
            {
                retVal = false;
            }
        }
        return retVal;
    }

    private bool isOnWallRigth()
    {
        bool retVal = false;
        float lenthToSearch = 0.1f;
        float colliderThreshold = 0.01f;

        Vector2 linestart = new Vector2(this.transform.position.x + GetComponent<Renderer>().bounds.extents.x + colliderThreshold, this.transform.position.y);

        Vector2 vectorToSearch = new Vector2(linestart.x + lenthToSearch, this.transform.position.y);

        RaycastHit2D hitRight = Physics2D.Linecast(linestart, vectorToSearch);

        retVal = hitRight;

        if (retVal)
        {
            if (hitRight.collider.GetComponent<NoSlideJump>())
            {
                retVal = false;
            }
        }
        return retVal;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "trap")
        {
            Instantiate(blood, transform.position, Quaternion.identity);
            Die();
            Invoke("ReloadScene", 2);
        }
    }

    private void Move(int dir)
    {
        float xValue = rigidbody2D.velocity.x;

        if (xValue < maxSpeed && dir > 0)
        {
            rigidbody2D.AddForce(movespeed * Time.deltaTime * Vector2.right * dir);
        }
        else if (xValue > -maxSpeed && dir < 0)
        {
            rigidbody2D.AddForce(movespeed * Time.deltaTime * Vector2.right * dir);
        }

        if (dir == 0)
        {
            rigidbody2D.Sleep();
        }
    }

    private void Flip (float Horizontal)
    {
        if (Horizontal > 0 && !facingRight || Horizontal < 0 && facingRight)
        {
            facingRight = !facingRight;

            Vector3 theScale = transform.localScale;

            theScale.x *= -1;

            transform.localScale = theScale;
        }
    }

    private void Die()
    {
        Debug.Log("You died");
        Died = true;
        gameObject.transform.position = spawner.transform.position;
    }

    private void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
