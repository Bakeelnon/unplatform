﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyableBlock : MonoBehaviour
{

    public Animator animator;
    public float DestroyRate = 15f;
    private float DestroyNext = 0f;
    private bool StartTime = false;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (StartTime)
        {
            DestroyNext += Time.time;
            if (DestroyNext > DestroyRate)
            {
                Destroy(gameObject);
            }
        }

       // Debug.Log(DestroyNext);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            StartTime = true;
            animator.SetBool("StartDestroy", true);
        }
    } 
}
