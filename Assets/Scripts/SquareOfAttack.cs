﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareOfAttack : MonoBehaviour
{
    public Animator animator;
    public bool inTheSquare = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }


    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            animator.SetBool("StartFire", true);
            inTheSquare = true;
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            animator.SetBool("StartFire", false);
            inTheSquare = false;
        }
    }
}
