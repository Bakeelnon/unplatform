﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_Camera : MonoBehaviour
{
    public Transform player;
    public Vector3 offset;
    private UN_PlayerController playerController;
    public GameObject Player;
    private bool PlayerDied;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        playerController = Player.GetComponent<UN_PlayerController>();
        PlayerDied = playerController.Died;
        if (!PlayerDied)
        {
            Die();
        }
        else
        {
            Invoke("DieWithAvoid", 2);
        }
    }

    void Die ()
    {
        transform.position = new Vector3(player.position.x + offset.x, player.position.y + offset.y, offset.z); // Camera follows the player with specified offset position
    }

    void DieWithAvoid ()
    {
        transform.position = new Vector3(player.position.x + offset.x, player.position.y + offset.y, offset.z); // Camera follows the player with specified offset position
        playerController.Died = false;
    }
}
