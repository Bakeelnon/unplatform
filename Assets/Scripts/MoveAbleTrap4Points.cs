﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAbleTrap4Points : MonoBehaviour
{
    // Transforms to act as start and end markers for the journey.
    public Transform startMarker;
    public Transform endMarker;
    public Transform secondMarker;
    public Transform thirdMarker;

    // Movement speed in units/sec.
    public float speed = 1.0F;

    // Time when the movement started.
    private float startTime;
    private float endTime;
    private float secondTime;
    private float thirdTime;

    // Total distance between the markers.
    private float journeyLength;
    private float reverseJourney;
    private float secondJourney;
    private float thirdJourney;

    //Point reach detecting
    private bool isStartMakerReached = true;
    private bool isEndMakerReached = false;
    private bool isSecondMarkerReached = false;
    private bool isThirdMarkerReached = false;

    void Start()
    {
        // Keep a note of the time the movement started.
        startTime = Time.time;

        // Calculate the journey length.
        journeyLength = Vector3.Distance(startMarker.position, endMarker.position);
        secondJourney = Vector3.Distance(endMarker.position, secondMarker.position);
        thirdJourney = Vector3.Distance(secondMarker.position, thirdMarker.position);
        reverseJourney = Vector3.Distance(endMarker.position, startMarker.position);
    }

    // Follows the target position like with a spring
    void Update()
    {
        if (isStartMakerReached)
        {
            MoveToEndMarker();
        }
        if (isEndMakerReached)
        {
            MoveToSecondMarker();
        }
        if (isSecondMarkerReached)
        {
            MoveToThirdMarker();
        }
        if (isThirdMarkerReached)
        {
            MoveToStartMarker();
        }
    }

    void MoveToEndMarker()
    {
        // Distance moved = time * speed.
        float distCovered = (Time.time - startTime) * speed;

        // Fraction of journey completed = current distance divided by total distance.
        float fracJourney = distCovered / journeyLength;

        // Set our position as a fraction of the distance between the markers.
        transform.position = Vector3.Lerp(startMarker.position, endMarker.position, fracJourney);

        //keep a entry point of the time movement started to Start position
        endTime = Time.time;

        if (transform.position == endMarker.position)
        {
            isEndMakerReached = true;
            isStartMakerReached = false;
        }
    }

    void MoveToStartMarker()
    {
        // Distance moved = time * speed.
        float distCovered = (Time.time - endTime) * speed;

        // Fraction of journey completed = current distance divided by total distance.
        float fracJourney = distCovered / reverseJourney;

        // Set our position as a fraction of the distance between the markers.
        transform.position = Vector3.Lerp(endMarker.position, startMarker.position, fracJourney);

        startTime = Time.time;

        if (transform.position == startMarker.position)
        {
            isEndMakerReached = false;
            isStartMakerReached = true;
        }

    }

    void MoveToSecondMarker()
    {
        // Distance moved = time * speed.
        float distCovered = (Time.time - startTime) * speed;

        // Fraction of journey completed = current distance divided by total distance.
        float fracJourney = distCovered / journeyLength;

        // Set our position as a fraction of the distance between the markers.
        transform.position = Vector3.Lerp(endMarker.position, secondMarker.position, fracJourney);

        //keep a entry point of the time movement started to Start position
        endTime = Time.time;

        if (transform.position == secondMarker.position)
        {
            isSecondMarkerReached = true;
            isEndMakerReached = false;
            isStartMakerReached = false;
            isThirdMarkerReached = false;
        }
    }

    void MoveToThirdMarker()
    {
        // Distance moved = time * speed.
        float distCovered = (Time.time - startTime) * speed;

        // Fraction of journey completed = current distance divided by total distance.
        float fracJourney = distCovered / journeyLength;

        // Set our position as a fraction of the distance between the markers.
        transform.position = Vector3.Lerp(secondMarker.position, startMarker.position, fracJourney);

        //keep a entry point of the time movement started to Start position
        endTime = Time.time;

        if (transform.position == startMarker.position)
        {
            isEndMakerReached = false;
            isStartMakerReached = true;
        }
    }
}
