﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    public LevelsController levelController;
    public string SceneName;
    public int UnlockLevel;

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            PlayerPrefs.SetInt("LevelsSave", UnlockLevel);
            SceneManager.LoadScene(SceneName);
        }
    }
}
