﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvisibleTrap : MonoBehaviour
{

    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            animator.SetBool("Activate", true);
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        animator.SetBool("Activate", false);
    }
}
