﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiringTrap : MonoBehaviour
{
    public GameObject SquareOFAttacking;
    public Rigidbody2D Bullet;
    public GameObject Player;
    public float RateOfFire = 0.5f;
    private float FireNext = 0.0f;
    private SquareOfAttack speedController;
    private bool Attack;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        speedController = SquareOFAttacking.GetComponent<SquareOfAttack>();
        Attack = speedController.inTheSquare;
        //Debug.Log(Attack);

        Rigidbody2D cloneOfBullet;
        if (Attack && Time.time > FireNext)
        {
            FireNext = Time.time + RateOfFire;
            Vector3 PlayerPos = Player.transform.position;

            //Creating a bullet that aiming to player
            cloneOfBullet = Instantiate(Bullet, transform.position, transform.rotation);

            //Moving to player
            cloneOfBullet.velocity = (PlayerPos - transform.position).normalized * 20.0f;
        }

    }

}
