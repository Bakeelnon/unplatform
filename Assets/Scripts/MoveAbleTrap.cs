﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAbleTrap : MonoBehaviour
{
    // Transforms to act as start and end markers for the journey.
    public Transform startMarker;
    public Transform endMarker;

    // Movement speed in units/sec.
    public float speed = 1.0F;

    // Time when the movement started.
    private float startTime;
    private float endTime;

    // Total distance between the markers.
    private float journeyLength;
    private float reverseJourney;

    //Point reach detecting
    private bool isStartMakerReached = true;
    private bool isEndMakerReached = false;

    void Start()
    {
        // Keep a note of the time the movement started.
        startTime = Time.time;

        // Calculate the journey length.
        journeyLength = Vector3.Distance(startMarker.position, endMarker.position);
        reverseJourney = Vector3.Distance(endMarker.position, startMarker.position);
    }

    // Follows the target position like with a spring
    void Update()
    {
        if (isStartMakerReached)
        {
            MoveToEndMarker();
        }
        if (isEndMakerReached)
        {
            MoveToStartMarker();
        }
    }

    void MoveToEndMarker()
    {
        // Distance moved = time * speed.
        float distCovered = (Time.time - startTime) * speed;

        // Fraction of journey completed = current distance divided by total distance.
        float fracJourney = distCovered / journeyLength;

        // Set our position as a fraction of the distance between the markers.
        transform.position = Vector3.Lerp(startMarker.position, endMarker.position, fracJourney);

        //keep a entry point of the time movement started to Start position
        endTime = Time.time;

        if (transform.position == endMarker.position)
        {
            isEndMakerReached = true;
            isStartMakerReached = false;
        }
    }

    void MoveToStartMarker()
    {
        // Distance moved = time * speed.
        float distCovered = (Time.time - endTime) * speed;

        // Fraction of journey completed = current distance divided by total distance.
        float fracJourney =  distCovered / reverseJourney;

        // Set our position as a fraction of the distance between the markers.
        transform.position = Vector3.Lerp(endMarker.position, startMarker.position, fracJourney);

        startTime = Time.time;

        if (transform.position == startMarker.position)
        {
            isEndMakerReached = false;
            isStartMakerReached = true;
        }
    }
}

