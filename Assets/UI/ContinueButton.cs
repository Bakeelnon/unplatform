﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContinueButton : MonoBehaviour
{
    public GameObject Panel;

    public void ContinueGame()
    {
        Panel.SetActive(false);
    }
}
