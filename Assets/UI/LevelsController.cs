﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelsController : MonoBehaviour
{
    public Button button2;
    public Button button3;
    public Button button4;
    public Button button5;
    public Button button6;
    public Button button7;
    public Button button8;
    public Button button9;
    public Button button10;
    public Button button11;
    public Button button12;
    public Button button13;
    public Button button14;
    public Button button15;

    private int LelelvsAbiable;

    void Update()
    {
        LelelvsAbiable = PlayerPrefs.GetInt("LevelsSave");
        if (LelelvsAbiable >= 2)
        {
            button2.interactable = true;
        }
        else
        {
            button2.interactable = false;
        }
        if (LelelvsAbiable >= 3)
        {
            button3.interactable = true;
        }
        else
        {
            button3.interactable = false;
        }
        if (LelelvsAbiable >= 4)
        {
            button4.interactable = true;
        }
        else
        {
            button4.interactable = false;
        }
        if (LelelvsAbiable >= 5)
        {
            button5.interactable = true;
        }
        else
        {
            button5.interactable = false;
        }
        if (LelelvsAbiable >= 6)
        {
            button6.interactable = true;
        }
        else
        {
            button6.interactable = false;
        }
        if (LelelvsAbiable >= 7)
        {
            button7.interactable = true;
        }
        else
        {
            button7.interactable = false;
        }
        if (LelelvsAbiable >= 8)
        {
            button8.interactable = true;
        }
        else
        {
            button8.interactable = false;
        }
        if (LelelvsAbiable >= 9)
        {
            button9.interactable = true;
        }
        else
        {
            button9.interactable = false;
        }
        if (LelelvsAbiable >= 10)
        {
            button10.interactable = true;
        }
        else
        {
            button10.interactable = false;
        }
        if (LelelvsAbiable >= 11)
        {
            button11.interactable = true;
        }
        else
        {
            button11.interactable = false;
        }
        if (LelelvsAbiable >= 12)
        {
            button12.interactable = true;
        }
        else
        {
            button12.interactable = false;
        }
        if (LelelvsAbiable >= 13)
        {
            button13.interactable = true;
        }
        else
        {
            button13.interactable = false;
        }
        if (LelelvsAbiable >= 14)
        {
            button14.interactable = true;
        }
        else
        {
            button14.interactable = false;
        }
        if (LelelvsAbiable >= 15)
        {
            button15.interactable = true;
        }
        else
        {
            button15.interactable = false;
        }
    }
}
