﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetLevels : MonoBehaviour
{
    public void ResetAllLevels()
    {
        PlayerPrefs.SetInt("LevelsSave", 0);
    }
}
