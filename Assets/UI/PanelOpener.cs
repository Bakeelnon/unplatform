﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelOpener : MonoBehaviour
{

    public GameObject LevelsPanel;
    public GameObject MainMenuPanel;
    // Start is called before the first frame update

    public void OpenPanel()
    {
        if (LevelsPanel != null)
        {
            LevelsPanel.SetActive(true);
            MainMenuPanel.SetActive(false);
        }
    }
}
