﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackButton : MonoBehaviour
{
    public GameObject Panel;
    public GameObject MainMenuPanel;

    public void BackPanel()
    {
        Panel.SetActive(false);
        MainMenuPanel.SetActive(true);
    }
}
